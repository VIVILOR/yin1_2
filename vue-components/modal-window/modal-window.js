Vue.component('modal-window', {
    template: `
    <transition name="modal-window">
        <div id="modal-window-wrapper" class="absolute">
            <div class="window relative" @click="onButtonClick">
                <div class="flex flex-column flex-auto">
                    <h2
                        v-if="_content.header"
                        class="header-text main-color flex-auto"
                        v-html="_content.header"
                    ></h2>
                    <span
                        class="default-text flex-auto"
                        v-html="_content.text"
                    ></span>
                </div>
                <div
                    v-for="(button, i) in _content.buttons"
                    :data-buttonIndex="i"
                    v-html="button.text"
                    :class="[
                        'button', 'button-text', 'relative',
                        'flex-initial',
                        button.type ? button.type : 'normal'
                    ]"
                ></div>
            </div>
        </div>
    </transition>
    `,
    props: {
        content: { required: true }
    },
    computed: {
        _content: function() {
            if (!this.content || typeof this.content !== "object") {
                return {
                    header: '',
                    text: '',
                    buttons: []
                }
            } else {
                return this.content;
            }
        }
    },
    methods: {
        onButtonClick: function(ev) {
            if (ev.target.className.indexOf('button') > -1) {
                this.$emit('button-click', ev.target.dataset.buttonindex);
            }
        }
    }

});