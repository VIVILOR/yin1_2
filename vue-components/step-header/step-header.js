Vue.component("step-header", {
    template: `
    <div id="step-header">
        <ul v-if="steps.count > 1">
            <li
                v-for="i in steps.count"
                v-html="i < steps.active ? '&#10004;' : '<b>'+ i +'</b>'"
                :class="[
                    'center',
                    'inline-block',
                    'default-text',
                    i > steps.active ? 'inactive' : 'active'
                ]"
            ></li>
        </ul>
        <transition name="step-header" type="in-out">
            <h2 v-show='header.show' class='subheader-text' v-html="header.text"></h2>
        </transition>
    </div>
    `,
    props: {
        content: {
            type: Array,
            required: true
        },
        activeStep: {
            required: true
        }
    },
    data: function() {
        return {
            header: {
                show: true,
                text: ''
            }
        }
    },
    computed: {
        steps: function() {
            return {
                count: this.content.length,
                active: this.activeStep
            }
        }
    },
    methods: {
        animateHeader: function(index) {
            this.header.show = false;
            setTimeout(() => {
                this.header.text = index > -1 ? this.content[index].header : '';
                this.header.show = true;
            }, 400);
        }
    },
    watch: {
        "steps.active": function (newValue) {
            if (newValue < 1) {
                this.animateHeader(-1)
            }
            else if (newValue > this.steps.count) {
                this.animateHeader(this.steps.count - 1)
            }
            else {
                this.animateHeader(newValue - 1)
            }
        }
    },
    mounted: function() {
        this.animateHeader(this.steps.active - 1);
    }
});