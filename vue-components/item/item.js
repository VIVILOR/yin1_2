Vue.component('item', {
    template: `
        <component
            v-html  = "_content.html"
            :is     = "_content.tag"
            :align  = "_content.attr.align"
            :alt    = "_content.attr.alt"
            :class  = "_content.attr.class"
            :hspace = "_content.attr.hspace"
            :id     = "_content.attr.id"
            :src    = "_content.attr.src"
            :style  = "_content.attr.style"
            :vspace = "_content.attr.vspace"
            :width  = "_content.attr.width"
            >
        </component>
    `,
    props: {
        content: {
            type: Object,
            required: true
        }
    },
    computed: {
        _content: function() { return this.content }
    }
});

