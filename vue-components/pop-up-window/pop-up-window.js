Vue.component('pop-up-window', {
    template: `
    <transition name="pop-up-window">
        <div id="pop-up-window" class="flex absolute window">
            <div
                class="align-self-center i-text bold main-color flex-auto"
                v-html="_content.text"
            ></div>
        </div>
    </transition>
    `,
    props: {
        content: { required: true }
    },
    computed: {
        _content: function() {
            if (!this.content)
                return { text: '' };
            else
                return this.content
        }
    }
});