const FG_CFG = {
    start: {
        x: {
            l: 7,
            r: 69
        },
        y: 78
    },
    delta: {
        x: 20,
        y: 37
    },
    errorClass: 'wrong',
    animationDuration: 500,
    animationTranslateX: 50
};


Vue.component('flasks-group', {
    template: `
    <div id="yin1-2-flasks-and-traps-wrapper">
        <div class="absolute"></div>
        <transition
            mode                ="in-out"
            name                ="traps-group"
            @after-enter        ="enableDragAndDrop"
            @after-leave        ="enableDragAndDrop"
            @enter-cancelled    ="enableDragAndDrop"
            @leave-cancelled    ="enableDragAndDrop"
        >
            <ul
                class   ="traps-group relative"
                v-show  ="traps.show"
            >
                <li
                    v-for   ="(trap,i) in traps.content"
                    :key    ="trap"
                    class   ="inline-block"
                >
                    <div
                        :class          ="trap[0]"
                        :data-trapindex ="i"
                    ></div>
                    <div
                        class   ="caption button-text center"
                        v-html  ="trap[1]"
                    ></div>
                    <div
                        v-if    ="trap[2]"
                        class   ="note button-text relative center bold"
                        v-html  ="trap[2]"
                    ></div>
                </li>
            </ul>
        </transition>
        <transition-group
            tag                 ="ul"
            name                ="flasks-group"
            class               ="flasks-group relative"
            @after-enter        ="enableDrag"
            @after-leave        ="enableDrag"
            @enter-cancelled    ="enableDrag"
            @leave-cancelled    ="enableDrag"
        >
            <li
                v-for="(flask, i) in flasksGroup"
                class="flask"
                :key="flask"
            >
                <svg v-show="!i && liquidVisible">
                    <polygon
                        :points="liquidLevelSVGContour()"
                        class="flask-liquid"
                    ></polygon>
                </svg>
            </li>
        </transition-group>
    </div>
    `,
    props: {
        prevT:          { required: true },
        appCfg:         { required: true },
        content:        { required: true },
        activeStep:     { required: true },
        liquidRatio:    { required: true },
    },
    computed: {
        _prevT:         function() { return Number(this.prevT); },
        _activeStep:    function() { return Number(this.activeStep); },
        _liquidRatio:   function() { return Number(this.liquidRatio); },
    },
    data: function() {
        let count, limitsObj;
                
        switch (this.activeStep) {
            case 0:  count = 0; break;
            case 1:  count = 1; break;
            default: count = 4; break;
        }
    
        this.next = count;
        
        return {
            // Component config
            cfg: FG_CFG,
            
            // Data used in template and animation (Vue)
            next:               count,
            count:              count,
            traps: {
                show:           true,
                content:        this.content[this.activeStep - 1]
            },
            flasksGroup:        Array.from(new Array(count), (v, i) => i),
            trapAllowed:        false,
            liquidVisible:      true,
            highlightError:     null,
            liquidRatioPrev:    0,
            // DOM-objects
            el: {
                flask:  null,
                traps:  null,
                trash:  null,
                flasks: null
            },
            
            // Trapped flask statuses
            trapped: {
                diesel:         false,
                kerosene:       false
            },
            
        }
    },
    methods: {
        liquidLevelSVGContour: function() {
            let ratio = this._liquidRatio;
            with (this.cfg) {
                let dx = delta.x * ratio;
                let dy = delta.y * ratio;
                return [
                    [
                        start.x.l,
                        start.y
                    ],
                    [
                        start.x.l + dx,
                        start.y - dy
                    ],
                    [
                        start.x.r - dx,
                        start.y - dy
                    ],
                    [
                        start.x.r,
                        start.y
                    ]
                ].join(' ', ',');
            }
        },
        
        shiftLeft: function() {
            this.flasksGroup.splice(0, 1);
            this.flasksGroup.splice(this.count, 0, this.next);
            this.next++;
        },
        
        shiftRight: function() {
            this.next--;
            this.flasksGroup.splice(this.count - 1, 1);
            this.flasksGroup.splice(0, 0, this.next - this.count);
        },
        
        setErrorHighlighting: function(args) {
            with (this) {
                switch (_activeStep) {
                    case 2:
                        $set(highlightError, 0, args[0]);
                        $set(highlightError, 1, args[1]);
                        break;
                }
            }
        },
        
        checkBlend: function() {
            with (this) {
                switch (_activeStep) {
                    case 2:
                        let eventValue, trapsToHighlight;
                        
                        if (trapped.kerosene) {
                                 if (_prevT > appCfg.T.d + appCfg.T.eps) { trapsToHighlight = [false, true];  eventValue =  1; }
                            else if (_prevT < appCfg.T.d - appCfg.T.eps) { trapsToHighlight = [false, true];  eventValue = -1; }
                            else                                         { trapsToHighlight = [false, false]; eventValue =  0; }
                        } else {
                                 if (_prevT > appCfg.T.k + appCfg.T.eps) { trapsToHighlight = [true, true];   eventValue =  1; }
                            else if (_prevT < appCfg.T.k - appCfg.T.eps) { trapsToHighlight = [true, false];  eventValue = -1; }
                            else                                         { trapsToHighlight = [false, false]; eventValue =  0; }
                        }
                        
                        $emit('flask-drag-start', eventValue);
                        setErrorHighlighting(trapsToHighlight);
                        break;
                    case 3:
                        $emit('flask-drag-start');
                        break;
                }
            }
        },
        
        enableDrag: function() {
            with (this) {
                if (_activeStep > 1) {
                    el.flask = $('.flasks-group li.flask:first-child').draggable({
                        scope:          "default",
                        cursor:         "grabbing",
                        zIndex:         "2000",
                        revertDuration: FG_CFG.animationDuration,
                        
                        helper: () => {
                            return getFlaskWithLiquid(el.flask.clone());
                        },
                        
                        revert: (droppable) => {
                            el.traps.removeClass(cfg.errorClass);
                            
                            if (droppable) {
                                if (trapAllowed) {
                                    return false;
                                }
                            }
        
                            new Promise(
                                (resolve) => {
                                    shiftRight();
                                    setTimeout(
                                        () => resolve(),
                                        cfg.animationDuration
                                    );
                                }
                            ).then(() => {
                                $emit('flask-drop', -2);
                                liquidVisible = true;
                            });
                            return true;
                        },
                        
                        start: (ev, ui) => {
                            liquidVisible = false;
                            
                            shiftLeft();
                            
                            checkBlend();
                        }
                    });
                }
            }
        },
        
        checkTrap: function(trapIndex) {
            with (this) {
                switch (_activeStep) {
                    case 2:
                        if (trapped.kerosene) {
                            trapAllowed = !highlightError[1];
                        } else {
                            switch (trapIndex) {
                                case 1: trapAllowed = !highlightError[0]; break;
                                case 2: trapAllowed = false; break;
                            }
                        }
                        break;
                }
            }
        },
        
        trapFlask: function(flask, trap) {
            let $flask = $(flask).clone().removeAttr('style');
            $(trap).append($flask);
            return $flask;
        },
        
        getFlaskWithLiquid: function(flask) {
            return $(flask).css('transition', 'none');
        },
        
        enableDragAndDrop: function() {
            with (this) {
                switch (_activeStep) {
                    case 2:
                        el.traps = $('ul.traps-group li:not(:first-child) div.trap').droppable({
                            scope: "default",
                            tolerance: "intersect",
                            drop: (ev, ui) => {
                                let trapIndex = Number(ev.target.dataset.trapindex);
                
                                checkTrap(trapIndex);
                                if (trapAllowed) {
                                    trapFlask(ui.helper, ev.target);
                    
                                    if (trapped.kerosene)
                                        trapped.diesel = true;
                                    else
                                        trapped.kerosene = true;
                    
                                    $emit("liquid-ratio-change", 0);
                                    liquidVisible = true;
                                }
                
                                $emit("flask-drop", trapIndex);
                
                            },
                            over: (ev) => {
                                if (highlightError[0] && highlightError[0] === highlightError[1]) {
                                    el.traps.addClass(this.cfg.errorClass);
                                    return;
                                }
                                if (Number(ev.target.dataset.trapindex) === 2 && !trapped.kerosene) {
                                    $(ev.target).addClass(cfg.errorClass);
                                    return;
                                }
                
                                if (highlightError[0]) $(el.traps[0]).addClass(cfg.errorClass);
                                if (highlightError[1]) $(el.traps[1]).addClass(cfg.errorClass);
                
                            },
                            out: () => {
                                el.traps.removeClass(cfg.errorClass);
                            }
                        });
                        break;
    
                    case 3:
                        el.traps = $('ul.traps-group li:first-child div.trap').droppable({
                            tolerance: "intersect",
                            drop: (ev, ui) => {
                                if (!trapAllowed)
                                    return;
                                
                                liquidRatioPrev = _liquidRatio;
                                
                                trapFlask(ui.helper, ev.target).draggable({
                                    cursor:         "grabbing",
                                    revertDuration: FG_CFG.animationDuration,
                                    scope:          "trapped",
                                    zIndex:         2000,
    
                                    helper: (ev) => {
                                        return getFlaskWithLiquid(ev.currentTarget)
                                    },
    
                                    revert: (droppable) => {
                                        if (droppable && _liquidRatio === 0) {
                                            return false
                                        } else {
                                            this.$emit("flask-drop", -2);
                                            return true;
                                        }
                                    },
                                    
                                    start: () => checkBlend()
                                });
                                
                                this.$emit("liquid-ratio-change", 0);
                                this.$emit("flask-drop", 0);
                                
                                $(ev.target).droppable('disable');
                                
                                liquidVisible = true;
                            }
                        });
                        
                        el.trash = $('ul.traps-group li:last-child div.trash').droppable({
                            tolerance: "intersect",
                            drop: (ev, ui) => {
                                $(ui.helper).remove();
                                this.$emit("liquid-ratio-change", 0);
                                this.$emit("flask-drop", -1);
                                liquidVisible = true;
                            }
                        });
                        
                        el.flasks = $('ul.flasks-group').droppable({
                            scope: "trapped",
                            tolerance: "intersect",
                            drop: (ev, ui) => {
                                
                                if (_liquidRatio > 0) {
                                    return;
                                }
                                
                                el.traps.droppable('enable');
                                trapAllowed = true;
                                
                                console.log(ui.helper.data('liquidratio'));
                                console.log(ui.draggable.data('liquidratio'));
                                let $flask = $(ui.helper);
                                let $flaskFix = $flask
                                    .clone()
                                    .removeAttr('style')
                                    .addClass('absolute')
                                    .css({top: '228px'});
                                
                                $('#yin1-2-flasks-and-traps-wrapper').append($flaskFix);
                                
                                shiftRight();
                                
                                setTimeout(() => $flaskFix.remove(), cfg.animationDuration);
    
                                $emit("liquid-ratio-change", liquidRatioPrev);
                                $emit("flask-drop", 1);
    
                                $flask.remove();
                                
                                liquidVisible = true;
                            }
                        });
                        break;
                }
            }
        },
        
        pendingSplice: function(newFlasksCount) {
            let _this = this;
            return new Promise((resolve) => {
                setTimeout(
                    () => {
                        _this.flasksGroup.splice(_this.count - 1, 0, _this.next);
                        resolve();
                    },
                    FG_CFG.animationDuration / 2
                );
            }).then(() => {
                newFlasksCount--;
                _this.next++;
                return newFlasksCount ?
                    _this.pendingSplice(newFlasksCount) :
                    null;
            })
        }
    },
    watch: {
        count: function(newVal, oldVal) {
            with (this) {
                promiseStorage = new Promise((resolve) => {
                    if (newVal > oldVal) {
                        new Promise((resolve) => {
                            pendingSplice(newVal - oldVal);
                            resolve();
                        }).then(() => resolve());
                    } else {
                        flasksGroup.splice(newVal, oldVal - newVal + 1);
                        next -= (oldVal - newVal);
                        resolve();
                    }
                }).then(() => {});
            }
            
        },
        _activeStep: function(newVal) {
            switch (newVal) {
                case 0:
                    this.count = 0;
                    break;
                case 1:
                    this.count = 1;
                    break;
                case 2:
                    new Promise(
                        (resolve) => {
                            this.count = 0;
                            resolve();
                        }
                    ).then(
                        () => {
                            this.count = 4;
                            this.trapped.kerosene   = false;
                            this.trapped.diesel     = false;
                        }
                    );
                    break;
                default:
                    this.trapAllowed = true;
                    this.count = 4;
                    break;
            }
            this.traps.show = false;
            setTimeout(
                () => this.traps.show = true,
                FG_CFG.animationDuration
            );
        },
        liquidRatio: function(v) {
            if (this._activeStep > 2) {
                this.trapAllowed = v > 0
            }
        },
        'traps.show': function(newVal) {
            if (newVal)
                this.traps.content = this.content[this.activeStep - 1];
        },
        'trapped.kerosene': function(v) {
            $(this.el.traps[0]).droppable(v ? 'disable' : 'enable');
        },
        'trapped.diesel': function(v) {
            if (v) this.$emit('complete');
        }
    },
    mounted: function() {
        with (this) {
            enableDrag();
            traps.show = true;
        }
    }
});