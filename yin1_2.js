const APP_CFG = {
    stove: {
        dt: 200,
        dT: 1,
        dTa: 4,
    },
    T: {
        eps:    2,
        min:    40,
        p:      140,
        k:      220,
        d:      350,
        max:    400
    }
};


let app = new Vue({
    el: "#app-wrapper",
    data: {
        content: CONTENT_RU,
        cfg:     APP_CFG,
        
        activeStep: 0,
        wasActionPerformed: false,
        T: {
            current: 0,
            range: 0,
            prev: 0,
            min: 0,
            max: 0
        },
        stove: {
            isWorking: false,
            isPaused: false,
            accelerateHeating: false,
        },
        liquid: {
            dV: 0,
            ratio: {
                heating: 0.0,
                output: 0.0
            },
            isDropping: false,
            wasDropping: false,
        },
        buttonType: 0,
        window: {
            modal: {
                type: -1,
                hidden: true
            },
            popUp: {
                type: -1,
                hidden: true
            }
        },
        evaporated: {
            diesel: false,
            kerosene: false,
            petroleum: false,
        },
        acceleratingLimit: null,
    },
    methods: {
        launchStoveProcess: function() {
            with (this) {
                setInterval(
                    () => {
                        if (!stove.isPaused) {
                            if (stove.isWorking) {
                                if (T.current < cfg.T.max) {
                                    if (stove.accelerateHeating) {
                                             if (evaporated.kerosene)   acceleratingLimit = 'k';
                                        else if (evaporated.petroleum)  acceleratingLimit = 'p';
                                        else                            acceleratingLimit = 'min';
                                        
                                        T.current +=
                                            T.current < cfg.T[acceleratingLimit] ? cfg.stove.dTa : cfg.stove.dT;
                                    } else {
                                        T.current += cfg.stove.dT;
                                    }
                                }
                            }
                            else {
                                if (T.current > 0)
                                    T.current -= cfg.stove.dT;
                            }
                        }
                    },
                    cfg.stove.dt
                );
            }
        },
        resetStep: function() {
            with (this) {
                switch (activeStep) {
                    case 1:
                        buttonType = 0;
                        stove.isPaused  = false;
                        stove.isWorking = false;
                        stove.accelerateHeating = false;
                        
                        T.prev      = 0;
                        T.current   = 0;
                        
                        liquid.ratio.output     = 0;
                        liquid.ratio.heating    = 0;
                        
                        break;
                        
                    case 2:
                        buttonType = 1;
                        
                        stove.isPaused  = false;
                        stove.isWorking = false;
                        stove.accelerateHeating = true;
                        
                        liquid.ratio.output = 0;
                        liquid.isDropping   = false;
                        liquid.wasDropping  = false;
                        
                        evaporated.diesel   = false;
                        evaporated.kerosene = false;
                        evaporated.petroleum = true;
                        
                        if (!(T.prev > cfg.T.p - cfg.T.eps && T.prev > cfg.T.p + cfg.T.eps))
                            liquid.ratio.heating += cfg.T.p * liquid.dV;
                            T.prev = cfg.T.p;
                        break;
                        
                    case 3:
                        buttonType = 0;
                        
                        stove.isPaused  = false;
                        stove.isWorking = false;
                        stove.accelerateHeating = true;
    
                        liquid.isDropping   = false;
                        liquid.wasDropping  = false;
                        liquid.ratio.output = 0;
                        liquid.ratio.heating = 0;
    
                        evaporated.diesel   = false;
                        evaporated.kerosene = false;
                        evaporated.petroleum = false;
    
                        T.current = 0;
                        T.prev = 0;
                        break;
                }
            }
        },
        
        showModalWindow: function(type) {
            with(this.window) {
                modal.type = type;
                modal.hidden = false;
            }
        },
    
        showPopUpWindow: function(type) {
            with(this.window) {
                popUp.type = type;
                popUp.hidden = false;
            }
        },
        
        /*
         * Event handlers (native and emitted by Vue)
         */
        
        onModalWindowButtonClick: function(buttonIndex) {
            this.window.modal.hidden = true;
            switch (this.activeStep) {
                case 1:
                    switch (this.window.modal.type) {
                        case 0:
                            this.activeStep = 2;
                            
                            $("#yin1-2-flasks-and-traps-wrapper").append(
                                $(".flask:first-child")
                                    .clone()
                                    .addClass("absolute")
                                    .css({top: "228px"})
                                    .addClass("flask-moving")
                            );
                            
                            break;
                        case 2:
                        case 8:
                            this.resetStep();
                            break;
                    }
                    break;
    
                case 2:
                    switch (this.window.modal.type) {
                        case 0:
                            this.activeStep = 3;
                            break;
                        case 5:
                            this.activeStep = 1;
                            break;
                    }
                    break;
                case 3:
                    switch (this.window.modal.type) {
                        case 7:
                            this.resetStep();
                            break;
                    }
            }
            this.unpause();
        },
        
        onControlButtonClick: function(ev) {
            let _this = this;
            this.pause();
            switch (_this.activeStep) {
                case 1:
                    switch (_this.buttonType) {
                        case 0:
                            if (_this.stove.isWorking)
                                _this.showModalWindow(1);
                            else if (_this.T.prev === 0)
                                _this.showModalWindow(4);
                            else if (_this.T.prev > _this.cfg.T.p + _this.cfg.T.eps)
                                _this.showModalWindow(2);
                            else if (_this.T.prev < _this.cfg.T.p - _this.cfg.T.eps)
                                _this.showModalWindow(3);
                            else
                                _this.showModalWindow(0);
                            break;
                    }
                    break;
                case 2:
                    switch (_this.buttonType) {
                        case 1:
                            _this.showModalWindow(5);
                            break;
                    }
                    break;
                case 3:
                    switch (_this.buttonType) {
                        case 0:
                            if (_this.stove.isWorking)
                                _this.showModalWindow(1);
                            else if (
                                _this.evaporated.petroleum &&
                                _this.evaporated.kerosene &&
                                _this.evaporated.diesel
                            ) {
                                _this.showModalWindow(0);
                            } else if (
                                (
                                    _this.T.prev < _this.cfg.T.p - _this.cfg.T.eps &&
                                    !_this.evaporated.petroleum
                                ) || (
                                    _this.T.prev < _this.cfg.T.k - _this.cfg.T.eps &&
                                    _this.evaporated.petroleum &&
                                    !_this.evaporated.kerosene
                                ) || (
                                    _this.T.prev < _this.cfg.T.d - _this.cfg.T.eps &&
                                    _this.evaporated.petroleum &&
                                    _this.evaporated.kerosene &&
                                    !_this.evaporated.diesel
                                )
                            ) {
                                _this.showModalWindow(6);
                            } else {
                                _this.showModalWindow(7);
                            }
                            break;
                    }
                    break;
            }
        },
        
        pause: function() {
            this.stove.isPaused = true;
            if (this.liquid.isDropping) {
                this.liquid.isDropping = false;
                this.liquid.wasDropping = true;
            }
        },
        
        unpause: function() {
            this.stove.isPaused = false;
            if (this.liquid.wasDropping) {
                this.liquid.wasDropping = false;
                this.liquid.isDropping = true;
            }
        },
        
        onFlaskDragStart: function(flaskStatus) {
            with (this) {
                pause();
                switch (activeStep) {
                    case 2:
                        if (flaskStatus < 0) {
                            showPopUpWindow(0)
                        } else if (flaskStatus > 0) {
                            showPopUpWindow(2)
                        } else {
                            if (this.T.prev < this.cfg.T.d - this.cfg.T.eps)
                                showPopUpWindow(1);
                            else
                                showPopUpWindow(3);
                        }
                        setTimeout(() => window.popUp.hidden = true, 5000);
                        break;
                }
            }
        },
        
        onFlaskDrop: function(index) {
            with (this) {
                window.popUp.hidden = true;
                switch (activeStep) {
                    case 2: // index here means trap index, where flask dropped
                        switch (index) {
                            case 1: evaporated.kerosene = true; break;
                            case 2: evaporated.diesel   = true; break;
                        }
                        break;
                        
                    case 3: // index here means place (not only traps)
                        switch (index) {
                            case -1:
                                if (T.prev <= cfg.T.k + cfg.T.eps && evaporated.petroleum)
                                    evaporated.kerosene = true;
                                break;
                            case 0:
                                if (T.prev <= cfg.T.p + cfg.T.eps && !evaporated.petroleum) {
                                    evaporated.petroleum = true;
                                } else if (
                                    T.prev >= cfg.T.d - cfg.T.eps &&
                                    evaporated.petroleum &&
                                    evaporated.kerosene
                                ) {
                                    evaporated.diesel = true;
                                }
                                break;
                            case 1:
                                break;
                        }
                        break;
                }
                unpause();
            }
        },
        
        onLiquidRatioChange: function(newRatio) {
            this.liquid.ratio.output = newRatio;
        },
        
        onComplete: function() {
            this.window.modal.type = 0;
            this.window.modal.hidden = false;
        }
    },
    mounted: function() {
        with(this) {
            T.range    = cfg.T.d - cfg.T.min;
            T.min      = cfg.T.min;
            T.max      = T.range + T.min;
            liquid.dV  = cfg.stove.dT / T.range;
            
            $('#yin1-2-control-button').click(onControlButtonClick);
            
            activeStep = 3;
            launchStoveProcess();
        }
    },
    watch: {
        "T.current": function(newV, oldV) {
            if (newV > oldV) {
                if (newV > this.T.prev &&
                    newV > this.T.min &&
                    newV < this.T.max) {
                    if (!this.liquid.isDropping) {
                        this.liquid.isDropping = true;
                    }
                    this.liquid.ratio.output    += this.liquid.dV * (newV - oldV);
                    this.liquid.ratio.heating   += this.liquid.dV * (newV - oldV);
                }
            }
        },
        activeStep: function() {
            this.$nextTick(
                () => { this.resetStep() }
            );
        },
        "stove.isWorking": function(newV, oldV) {
            if (newV) {
                this.stove.isWorking = true;
            } else {
                this.T.prev = this.T.current;
                this.stove.isWorking    = false;
                this.liquid.isDropping  = false;
            }
        }
    }
});