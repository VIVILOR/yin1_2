Vue.component('cjaf-base', {
    template: '\
        <div\
            v-if="visible">\
            <div\
                v-if   ="showTag"\
                :class ="labelTagClass">\
            </div>\
            <template\
                v-for = "part in reactiveContent">\
                <component\
                    v-html  = "part._html"\
                    :is     = "part._tag"\
                    :align  = "part._align"\
                    :alt    = "part._alt"\
                    :class  = "part._class"\
                    :hspace = "part._hspace"\
                    :id     = "part._id"\
                    :src    = "part._src"\
                    :style  = "part._style"\
                    :vspace = "part._vspace"\
                    :width  = "part._width"\
                ></component>\
            </template>\
         </div>\
        ',
    props: {
        content: {
            type: Array,
            required: true
        },
        prefix: {
            type: String,
            default: 'cjaf'
        },
        correct: {
            type: Boolean,
            default: true
        },
        showTag: {
            type: Boolean,
            default: false
        },
        highlight: {
            type: Boolean,
            default: false
        },
        visible: {
            type: Boolean,
            default: true
        }
    },

    computed: {
        labelTagClass: function() {
            let obj = {};
            obj[this.prefix + "-label-tag"] = true;
            obj[this.prefix + "-label-tag-wrong"] = !this.correct && this.highlight;
            return obj;
        },
        reactiveContent: function() {
            return this.content
        }
    }
});