
class LogicalExpression {
    constructor(__ops__) {
        if (__ops__.negation !== undefined) {
            this.negation = __ops__.negation;
        }
        
        this.expressionString = '';
        if (__ops__.expression !== undefined &&
            typeof __ops__.expression === 'string') {
            
        }
        
        this.operands = [];
        this.operations = [];
    }
    
    addExpression(expString) {
    
    }
}

class LexicalAnalyzer {
    constructor(__ops__) {
        let regExpFlags = [];
        
        if (__ops__.ignoreCase !== undefined) {
            this.ignoreCase = __ops__.ignoreCase;
            if (this.ignoreCase)
                regExpFlags.push('i');
        }
        
        this.operators = "()&|>";
        
        this.patterns = {
            indetifier: new RegExp(
                /[A-Za-z_]\w*/,                  // [A-Za-z0-9_]+
                regExpFlags.join('')
            ),
            values: '01',
        }
    }
    
    static checkBrackets(str) {
        let status = 0;
        
        str
            .split('')
            .forEach(function(c) {
                if (c === '(') status++;
                if (c === ')') status--;
            });
        
        return Boolean(status);
    }
}
