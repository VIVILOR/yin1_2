/*
*/

let CONTENT_RU = {
    header: "Разделение нефти на фракции",
    steps: [
        {
            header: 'Выделите бензиновую фракцию',
            content: {
                info: {
                    tag: "div",
                    html: `
                        Бензиновая фракция испаряется из нефти при температуре не более 140 °C.<br><br>
                        Выделите бензиновую фракцию: включите плитку и прогрейте нефть до необходимой температуры.
                    `,
                    attr: {
                        class: "window info-window button-text absolute",
                        id: "yin1-2-step1-info"
                    }
                }
            },
        },
        {
            header: 'Поэкспериментируйте с установкой<br> и попробуйте получить все возможные фракции.',
            content: {
            }
        },
        {
            header: 'Теперь попробуйте выделить смесь бенизиновой и дизельной фракций.',
            content: {
                info: {
                    tag: "div",
                    html: `
                        бензиновая<br>фракция
                        <br><br>
                        <b>до 140 &deg;C</b>
                        <br><br>
                        керосиновая<br>фракция
                        <br><br>
                        <b>140-220 &deg;C</b>
                        <br><br>
                        дизельная<br>фракция
                        <br><br>
                        <b>220-350 &deg;C</b>
                    `,
                    attr: {
                        class: "window info-window button-text center absolute",
                        id: "yin1-2-step3-info"
                    }
                }
            }
        },
    ],
    common: {
        traps: [
            [
                [ 'trap', 'бензиновая<br>фракция']
            ],
            [
                [ 'trap', 'бензиновая<br>фракция', 'до 140 &deg;C'],
                [ 'trap', 'керосиновая<br>фракция', '140-220 &deg;C'],
                [ 'trap', 'дизельная<br>фракция', '220-350 &deg;C'],
            ],
            [
                [ 'trap', '' ],
                [ 'trash', '' ]
            ]
        ],
        equipment: [
            {
                tag: "div",
                attr: { id: "yin1-2-heating-flask-liquid", class: "absolute" }
            },
            {
                tag: "div",
                attr: { id: "yin1-2-petroleum-heating-equipment", class: "absolute" }
            }
        ],
        window: {
            modal: [
                {
                    text: 'Все правильно',
                    buttons: [ { text: 'Продолжить' } ]
                },
                {
                    text: 'Вы забыли выключить плитку!',
                    buttons: [ { text: 'Ок' } ]
                },
                {
                    text: 'Вы перегрели колбу!',
                    buttons: [ { text: 'Начать шаг заново', type: "danger" } ]
                },
                {
                    text: 'Вы собрали недостаточное количество<br>нужной фракции',
                    buttons: [ { text: "Oк" } ],
                },
                {
                    text: 'Вы не выделили нужную фракцию.<br>Попробуйте включить плитку и нагреть нефть',
                    buttons: [ { text: "Oк" } ],
                },
                {
                    text: 'Начать задание заново?<br>Весь прогресс будет потерян!',
                    buttons: [ { text: "Да" }, { text: "Нет", type: "danger" } ],
                },
                {
                    text: 'Что-то не так.<br>Попробуйте продолжить эксперимент',
                    buttons: [ { text: "Продолжить" } ],
                },
                {
                    text: 'Что-то не так. Попробуйте еще раз',
                    buttons: [ { text: 'Начать шаг заново', type: "danger" } ]
                },
                {
                    text: 'Начать шаг заново?',
                    buttons: [ { text: "Да" }, { text: "Нет", type: "danger" } ],
                }
            ],
            popUp: [
                { text: 'Вы собрали недостаточное количество нужной фракции.<br>Продолжайте нагревать колбу.' },
                { text: 'Перенесите колбу с керосином на свое место.' },
                { text: 'Вы собрали в одной колбе две фракции.<br>Придется начать заново.' },
                { text: 'Перенесите колбу с дизелем на свое место.' },
            ]
        },
        buttons: [
            { text: "Проверить" },
            { text: 'Начать шаг заново', type: "danger" }
        ]
    }
};